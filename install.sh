#!/bin/bash 

set -xe

cargo build --release
cp target/release/todoer ~/.local/bin/todoer
