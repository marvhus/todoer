# TODOER
A tool for finding notes/todos/etc in comments in your codebase.

# Usage
## A single file
```console
$ todoer path/to/file.rs
...
```
## A folder
(checks all code files in folder and sub folders)
```console 
$ todoer path/to/folder 
...
```

Thanks to [FoxIDK](https://github.com/FoxIDK) for coming up with the name `TODOER`
