
// @Test This is a test
// @FixMe you should fix this
// @Todo @FIXME @fixme
// @Test @Test @Temporary @Hack

use std::{
    vec::Vec,
    iter::Peekable, 
    io::Write
};
use core::fmt;
use walkdir::WalkDir;

fn read_file(path: &str) -> String {
    std::fs::read_to_string(path)
        .expect("Failed to read file")
        .to_lowercase()
}

struct Annotation {
    name: String,
    comment_text: String,
    line: usize,
    col: usize,
}
impl Annotation {
    fn new(name: String, comment_text: String, line: usize, col: usize) -> Self {
        Self {
            name,
            comment_text,
            line,
            col,
        }
    }
}
impl fmt::Display for Annotation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}: @{}:\n{}", self.line, self.col, self.name, self.comment_text)
    }
}

struct Parser<Chars: Iterator<Item=char>> {
    chars: Peekable<Chars>,
    line: usize,
    col: usize,
}
impl<Chars: Iterator<Item=char>> Parser<Chars> {
    fn new(chars: Chars) -> Self {
        Self { 
            chars: chars.peekable(), 
            line: 1, 
            col: 0 
        }
    }
    fn parse_annotation(&mut self) -> String {
        let mut name = String::new();
        while let Some(x) = self.chars.next_if(|x| !x.is_whitespace()) {
            self.col += 1;
            name.push(x);
        }
        name
    }

    fn parse(&mut self, annotations: &mut Vec<(Annotation, String)>, path: String) {
        'annotations: loop {
            while let Some(x) = self.chars.next_if(|x| x.is_whitespace()) {
				self.col += 1;
				if x == '\n' {
					self.line += 1;
					self.col = 0;
				}
            }
            if let Some(x) = self.chars.next() {
                self.col += 1;
                match x {
					'"' => { // ignore strings
						if self.chars.peek() == Some(&'\'') { self.col += 1; continue 'annotations; } // in case there is a " in a char ('"')
						// ^ @FIXME this may cause issues if the string is "' ..."
						while let Some(x) = self.chars.next_if(|x| x != &'"') {
							self.col += 1;
							if x == '\n' {
								self.line += 1;
								self.col = 0;
							}
						}
						continue 'annotations;
					}
                    '/' => {
                        if let Some(x) = self.chars.next() {
                            self.col += 1;
                            match x {
                                '/' => {
                                    let mut line_text = String::from("//");
                                    let mut tmp: Vec<(String, usize, usize)> = Vec::new();
                                    'comment: while let Some(x) = self.chars.next() {
                                        line_text.push(x);
                                        self.col += 1;
                                        match x {
                                            '@' => {
                                                let name = self.parse_annotation();
                                                line_text += &name;
                                                tmp.push((name, self.line, self.col));
                                                continue 'comment;
                                            },
                                            '\n' => {
                                                line_text.pop();
                                                for (name, li, co) in &tmp {
                                                    annotations.push((
                                                        Annotation::new(name.clone(), line_text.clone(), *li, *co),
                                                        path.clone()
                                                    ));
                                                }

                                                self.col = 0;
                                                self.line += 1;
                                                continue 'annotations;
                                            },
                                            _ => {}
                                        }
                                    }
                                },
                                _ => continue 'annotations
                            }
                        }
                    },
                    _ => continue 'annotations
                }
            }
            break 'annotations;
        }
    }
}

fn display(annotations: &Vec<(Annotation, String)>) {
    let mut words: Vec<String> = Vec::new();
    for (annotation, _) in annotations.iter() {
        if !words.contains(&annotation.name) {
            words.push(annotation.name.clone())
        }
    }
    for word in words.iter() {
        let count = annotations.iter().filter(|(n, _)| n.name == *word).count();
        println!("{:-<15}: {}", word, count);
    }
}

fn parse_path(path: &str) -> Vec<(Annotation, String)> {
    let mut annotations: Vec<(Annotation, String)> = Vec::new();

    let supprted: Vec<&str> = vec!["rs", "c", "cc", "cpp", "txt"]; 
    // @Temporary the txt extension is only for testing
    for entry in WalkDir::new(path)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.path().is_file()) 
        .filter(|e| supprted.contains(&e.path().extension().unwrap().to_str().unwrap()))
    {
        let file_path = entry.path().to_str().unwrap();
        let content = read_file(file_path);
        Parser::new(content.chars()).parse(&mut annotations, file_path.to_string());
    }

    annotations
}

fn main() {
    let args: Vec<String> = std::env::args().collect(); 
    if args.len() < 2 {
        println!("Too few arguments. Expected: `todoer [path]`");
        std::process::exit(1);
    }

    let annotations = parse_path(&args[1]);

    display(&annotations);

    println!("_________________");
    let mut word = String::new();
    print!("Word > ");
    std::io::stdout().flush().unwrap();
    std::io::stdin().read_line(&mut word).expect("Failed to get user input");
    word = word.chars().filter(|x| !x.is_whitespace()).collect();

    println!("--- Occurances:");
    for (annotation, path) in annotations.iter().filter(|(x, _)| x.name == *word) {
        println!("{}:{}", path, annotation);
    }
}
